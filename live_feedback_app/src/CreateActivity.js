import React from 'react';
import axios from 'axios';
import AddDateToActivity from './AddDateToActivity'

class CreateActivity extends React.Component{
    
    constructor(props) {
        super(props);
        this.state = {
            activityID: '',
            description: '',
            endTime: ''
        };
    }
    
    createActivityID = (event) => {
        this.setState({activityID: event.target.value})
    }
    
    createDescription = (event) => {
        this.setState({description: event.target.value})
    }
    
    handleCreatedID = () => {

        const newActivity = Object.assign({}, this.state);
        
        axios.get('http://18.191.29.186:8080/api/activity').then(activities => {
            this.setState({
            activities: activities.data,
        })
        
        const IDs = this.state.activities.map(function(item, i){
        return item.activityID;
        })
        
        console.log("IDS: ");
        console.log(IDs);
        console.log("New ID: ");
        console.log(parseInt(newActivity.activityID, "10"));
        
        var check = false;
        
        for(let i = 0; i < IDs.length; i++){
            if(parseInt(IDs[i], "10")===parseInt(newActivity.activityID, "10")){
                check=true;
            }
        }
        
        if(check){
            window.alert('Activity Code Already Exists');
        }
        else{
            axios.post('http://18.191.29.186:8080/api/activity', newActivity).then(res => {
                //this.props.onActivityAdded(newActivity);
                window.alert('Activity Code hass been created.');
            }).catch(err => {
                console.log(err);
            })
        }
        })
    }
    
    onDateAdded = (date) => {
            var dateBuilder;
            var newDate = date;
            if(newDate.hours>=0&&newDate.hours<=9){
                if(newDate.minutes>=0 && newDate.minutes<=9){
                dateBuilder = `${newDate.date} 0${newDate.hours}:0${newDate.minutes}`
                }
                else{
                    dateBuilder = `${newDate.date} 0${newDate.hours}:${newDate.minutes}`
                }
            }
            else{
                if(newDate.minutes>=0 && newDate.minutes<=9){
                    dateBuilder = `${newDate.date} ${newDate.hours}:0${newDate.minutes}`
                }
                else{
                    dateBuilder = `${newDate.date} ${newDate.hours}:${newDate.minutes}`
                }
            }
            console.log("DATE:");
            console.log(dateBuilder);
            this.setState({endTime: dateBuilder});
        
    }
    
    render(){
        return(
        <div>
            <input placeholder="Activity ID" type="number" value={this.state.activityID} onChange={this.createActivityID}/>
            <input placeholder="Activity Description" value={this.state.description} onChange={this.createDescription}/>
            <AddDateToActivity addDate={this.onDateAdded}/>
            <button onClick={this.handleCreatedID}>Create Activity Code</button>
        </div>
        );
    }
}

export default CreateActivity;