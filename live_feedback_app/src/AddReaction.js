import React from 'react';
import axios from 'axios';
import sad_face from './media/sad face.png';
import happy_face from './media/smile face.png';
import confused_face from './media/confused face.png';
import shocked_face from './media/surprised face.png';



class AddReaction extends React.Component{
    
    constructor(props) {
        super(props);
        this.state = {
            reaction: '',
            timestamp: '',
            activityCode: ''
        };
    }
    
    addHappyReaction = () => {
        var date = new Date().getDate(); //Current Date
        var month = new Date().getMonth() + 1; //Current Month
        var year = new Date().getFullYear(); //Current Year
        var hours = new Date().getHours(); //Current Hours
        var min = new Date().getMinutes(); //Current Minutes
        //var sec = new Date().getSeconds(); //Current Seconds
        
        var dateBuilder = `${year}-${month}-${date} ${hours}:${min}`
        
        this.setState({reaction: 'HAPPY', timestamp: dateBuilder}, () => {
            this.handleAddReaction();
        });
    }
    
    addSadReaction = () => {
        var date = new Date().getDate(); //Current Date
        var month = new Date().getMonth() + 1; //Current Month
        var year = new Date().getFullYear(); //Current Year
        var hours = new Date().getHours(); //Current Hours
        var min = new Date().getMinutes(); //Current Minutes
        //var sec = new Date().getSeconds(); //Current Seconds
        
        var dateBuilder = `${year}-${month}-${date} ${hours}:${min}`
        this.setState({reaction: 'SAD', timestamp: dateBuilder}, () => {
            this.handleAddReaction();
        });
    }
    
    addConfusedReaction = () => {
        var date = new Date().getDate(); //Current Date
        var month = new Date().getMonth() + 1; //Current Month
        var year = new Date().getFullYear(); //Current Year
        var hours = new Date().getHours(); //Current Hours
        var min = new Date().getMinutes(); //Current Minutes
        //var sec = new Date().getSeconds(); //Current Seconds
        
        var dateBuilder = `${year}-${month}-${date} ${hours}:${min}`
        this.setState({reaction: 'CONFUSED', timestamp: dateBuilder}, () => {
            this.handleAddReaction();
        });
    }
    
    addShockedReaction = () => {
        var date = new Date().getDate(); //Current Date
        var month = new Date().getMonth() + 1; //Current Month
        var year = new Date().getFullYear(); //Current Year
        var hours = new Date().getHours(); //Current Hours
        var min = new Date().getMinutes(); //Current Minutes
        //var sec = new Date().getSeconds(); //Current Seconds
        
        var dateBuilder = `${year}-${month}-${date} ${hours}:${min}`
        this.setState({reaction: 'SHOCKED', timestamp: dateBuilder}, () => {
            this.handleAddReaction();
        });
    }
    
    handleAddReaction = () => {
        const feedback = Object.assign({}, this.state);
        console.log(feedback);
        axios.post('http://18.191.29.186:8080/api/feedback', feedback).then(res => {
            //this.props.onReactionAdded(feedback);
        }).catch(err => {
            console.log(err);
        })
    }
    
    handleActivityCode = (event) => {
        this.setState({activityCode: event.target.value});
    }
    handleInputCode = () => {
        axios.get('http://18.191.29.186:8080/api/activity').then(activities => {
            this.setState({
            activities: activities.data,
        })
        
        this.setState({activityClicked: false})
        
        const IDs = this.state.activities.map(function(item, i){
            return item.activityID;
        })
        
        const dates = this.state.activities.map(function(item, i){
            return item.endTime;
        })
        
        console.log("DATES:");
        console.log(dates);
        var check = false;
        var activityDate;
        for(let i = 0; i < IDs.length; i++){
            if(parseInt(IDs[i], "10")===parseInt(this.state.activityCode, "10")){
                check=true;
                activityDate=dates[i];
            }
        }
        
        if(!check){
            window.alert('Activity Code does not exist!');
        }
        else{
            var date = new Date().getDate(); //Current Date
            var month = new Date().getMonth() + 1; //Current Month
            var year = new Date().getFullYear(); //Current Year
            var hours = new Date().getHours(); //Current Hours
            var min = new Date().getMinutes(); //Current Minutes
            //var sec = new Date().getSeconds(); //Current Seconds
        
            var dateBuilder = `${year}-${month}-${date} ${hours}:${min}`
            if(activityDate<=dateBuilder){
                window.alert('Activity has expired!');
            }
            else{
                this.setState({activityClicked: true});
            }
        }
        })
        
    }
  
  render(){
        return(
            
        <div>
            <input placeholder="Activity ID" type="number" value={this.state.activityCode} onChange={this.handleActivityCode}/>
            <button onClick={this.handleInputCode}>Input Activity Code</button>
            {this.state.activityClicked === true &&
            <div>
                <img src={sad_face} alt="sad face" onClick={this.addSadReaction}/>
                <img src={happy_face} alt="happy face" onClick={this.addHappyReaction}/>
                <img src={confused_face} alt="confused face" onClick={this.addConfusedReaction}/>
                <img src={shocked_face} alt="shocked_face face" onClick={this.addShockedReaction}/>
             </div>}
         </div>
        );
  }
}

export default AddReaction;