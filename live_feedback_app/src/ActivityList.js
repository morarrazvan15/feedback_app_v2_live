import React from 'react';

class ActivityList extends React.Component {
    
    constructor(props) {
        super(props);
        this.state = {};
    }
    
    render() {
        let items = this.props.activities.map((activity, index) => <div key={index}>ActivityID: {activity.activityID} {activity.description} End Time: {activity.endTime} </div>);
        console.log(items);
        return (
            <div>
                {items}
            </div>
        )
    }
}

export default ActivityList;