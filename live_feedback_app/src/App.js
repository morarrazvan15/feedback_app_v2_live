import React, { Component } from 'react';
import './App.css';
import AddReaction from './AddReaction'
import FeedbackList from './FeedbackList'
import ActivityList from './ActivityList'
import CreateActivity from './CreateActivity'
import axios from 'axios';

class App extends Component {
  
  constructor(props) {
        super(props);
        this.state = {
            feedbacks: [],
            activities: []
        };
    }
    
  
  handleStudent = () => {
    this.setState({seeList: false});
    this.setState({seeActivityList: false});
    this.setState({user: 'student'});
  }
  
  handleTeacher = () => {
    this.setState({user: 'teacher'});
  }
  
  componentDidMount = () => {;
    setInterval(() => {
      axios.get('http://18.191.29.186:8080/api/feedback').then(feedbacks => {
      this.setState({
        feedbacks: feedbacks.data
      })
    })
    axios.get('http://18.191.29.186:8080/api/activity').then(activities => {
      this.setState({
        activities: activities.data
      })
    })
    axios.get('http://18.191.29.186:8080/api/feedback').then(activity_code => {
      this.setState({
        activity_id: activity_code.data
      })
    })
    }, 5000)
  }
  
  seeList = () => {
    this.setState({seeList: true});
    this.setState({seeActivityList: false});
  }
  
  seeActivityList = () => {
    this.setState({seeList: false});
    this.setState({seeActivityList: true});
  }
  
  render() {
    return (
      <div className="App">
        <button onClick={this.handleStudent}>Student</button>
        <button onClick={this.handleTeacher}>Teacher</button>
        {this.state.user === "student" && <AddReaction onReactionAdded={this.onReactionAdded}/>}
        {this.state.user === "teacher" === true && 
        <div>
        <CreateActivity onActivityAdded={this.onActivityAdded}/>
        <button onClick={this.seeList}>SEE FEEDBACK LIST</button>
        <button onClick={this.seeActivityList}>SEE ACTIVITY LIST</button>
        </div>}
        {this.state.seeList === true && <FeedbackList feedbacks={this.state.feedbacks}/>}
        {this.state.seeActivityList === true && <ActivityList activities={this.state.activities}/>}
      </div>
    );
  }
}

export default App;