import React from 'react';

class FeedbackList extends React.Component {
    
    constructor(props) {
        super(props);
        this.state = {};
    }
    
    render() {
        let items = this.props.feedbacks.map((feedback, index) => <div key={index}>{feedback.reaction}              {feedback.timestamp}                ActivityCode:{feedback.activityCode}</div>);
        console.log(items);
        return (
            <div>
                <h2>Reaction    Timestamp       ActivityCode</h2>
                {items}
            </div>
        )
    }
}

export default FeedbackList;