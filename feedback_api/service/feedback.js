const { Feedback } = require('./../models/feedback');

const feedback = {
    create: async (feedback) => {
        try {
            const result = await Feedback.create(feedback);
            return result;    
        } catch(err) {
           throw new Error(err.message); 
        }
    },
    getAll: async () => {
        try {
            const feedbacks = await Feedback.findAll();
            return feedbacks;
        } catch(err) {
            console.log("AICI");
            throw new Error(err.message);
        }
    },
    getByActivityCode: async (activityCode) => {
        try {
            const feedbacks = await Feedback.findAll({activityCode: activityCode});
            return feedbacks;
        } catch(err) {
            throw new Error(err.message);
        }
    }
}

module.exports = feedback;